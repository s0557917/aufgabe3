class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.string :product
      t.integer :reservation_number
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end
