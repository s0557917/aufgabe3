class ReservationsController < ApplicationController

  def create
    @customer = Customer.find(params[:customer_id])
    @reservation = @customer.reservations.create(reservation_params)
    redirect_to customer_path(@customer)
  end

  def destroy
    @customer = Customer.find(params[:customer_id])
    @reservation = @customer.reservations.find(params[:id])
    @reservation.destroy
    redirect_to customer_path(@customer)
  end
 
  private
    def reservation_params
      params.require(:reservation).permit(:product, :reservation_number)
    end

end
