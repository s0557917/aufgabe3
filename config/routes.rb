Rails.application.routes.draw do
  get 'welcome/index'

  resources :customers do 
  	resources :reservations
  end	

  root 'welcome#index'
end
